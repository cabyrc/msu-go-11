package main

import (
	"sort"
	"strconv"
)

func ReturnInt() int {
	return 1
}

func ReturnFloat() float32 {
	return 1.1
}

func ReturnIntArray() [3]int {
	data := [3]int{1, 3, 4}

	return data
}

func ReturnIntSlice() []int {
	data := []int{1, 2, 3}

	return data
}

func IntSliceToString(input []int) string {
	result := ""

	for _, val := range input {
		//result += fmt.Sprintf("%v", val)
		result += strconv.Itoa(val)
	}

	return result
}

func MergeSlices(inputFloat32 []float32, inputInt32 []int32) []int {
	tmp := make([]int, len(inputFloat32)+len(inputInt32))

	for key, val := range inputFloat32 {
		tmp[key] = int(val)
	}

	offset := len(inputFloat32)

	for key, val := range inputInt32 {
		tmp[key+offset] = int(val)
	}

	return tmp
}

func GetMapValuesSortedByKey(values map[int]string) []string {
	result := make([]string, len(values), len(values))
	tmp := make([]int, len(values))

	var idx int

	for key := range values {
		tmp[idx] = key
		idx += 1
	}

	sort.Ints(tmp)

	for key, val := range tmp {
		result[key] = values[val]
	}

	return result
}
