package bot

import (
	"gopkg.in/telegram-bot-api.v4"
	"log"
	"net/http"
)

type TGBot struct {
	bot        *tgbotapi.BotAPI
	port       string
	webhookURL string
	commands   map[string](func(*tgbotapi.Update) string)
}

func (tgbot *TGBot) RegisterCommand(name string, fn func(update *tgbotapi.Update) string) {
	tgbot.commands[name] = fn
}

func (tgbot *TGBot) Run() {
	// Heroku прокидывает порт для приложения в переменную окружения PORT
	tgbot.bot.Debug = true

	log.Printf("Authorized on account %s", tgbot.bot.Self.UserName)

	// Устанавливаем вебхук
	_, err := tgbot.bot.SetWebhook(tgbotapi.NewWebhook(tgbot.webhookURL))
	if err != nil {
		log.Fatal(err)
	}

	updates := tgbot.bot.ListenForWebhook("/")
	go http.ListenAndServe(":"+tgbot.port, nil)

	// получаем все обновления из канала updates
	for update := range updates {
		log.Println("received text: ", update.Message.Text)

		if update.Message.IsCommand() {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")

			if fn, ok := tgbot.commands[update.Message.Command()]; ok {
				msg.Text = fn(&update)
			} else {
				msg.Text = "I don't know that command"
			}

			tgbot.bot.Send(msg)
		}
	}
}

func (tgbot *TGBot) SendMessage(chatId int64, text string) {
	msg := tgbotapi.NewMessage(chatId, text)
	tgbot.bot.Send(msg)
}

func (tgbot *TGBot) Init(webhookURL string, port string, apiKey string) {
	bot, err := tgbotapi.NewBotAPI(apiKey)
	if err != nil {
		log.Fatal(err)
	}
	tgbot.port = port
	tgbot.bot = bot
	tgbot.commands = map[string](func(update *tgbotapi.Update) string){}
	tgbot.webhookURL = webhookURL
}
