package main

import (
	"bot"
	"fmt"
	"game"
	"gopkg.in/telegram-bot-api.v4"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"
)

type GameBot struct {
	bot.TGBot
	game                          *game.Game
	commands                      map[string]game.PlayerCommand
	registeredPlayers             map[int]*game.Player
	registeredPlayerChatIds       map[int]int64
	registeredPlayerIds           map[string]int
	registeredPlayerInputChannels map[int]chan string
}

func waitExitSignal() {
	// Go signal notification works by sending `os.Signal`
	// values on a channel. We'll create a channel to
	// receive these notifications (we'll also make one to
	// notify us when the program can exit).
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)

	// `signal.Notify` registers the given channel to
	// receive notifications of the specified signals.
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	// This goroutine executes a blocking receive for
	// signals. When it gets one it'll print it out
	// and then notify the program that it can finish.
	go func() {
		sig := <-sigs
		fmt.Println()
		fmt.Println(sig)
		done <- true
	}()

	// The program will wait here until it gets the
	// expected signal (as indicated by the goroutine
	// above sending a value on `done`) and then exit.
	fmt.Println("awaiting signal")
	<-done
	fmt.Println("exiting")
}

func (gb *GameBot) deregisterByUserId(userId int) string {
	if gb.registeredPlayers[userId] == nil {
		return "Вы не зарегистрированы в игре"
	}

	var playerName string
	for name, id := range gb.registeredPlayerIds {
		if id == userId {
			playerName = name
			break
		}
	}

	gb.deregisterPlayerByName(playerName)

	return "Вы вышли из игры"
}

func (gb *GameBot) deregisterPlayerByName(name string) string {
	userId := gb.registeredPlayerIds[name]

	if userId == 0 {
		return "Пользователь не найден"
	}

	gb.SendMessage(gb.registeredPlayerChatIds[userId], "Вы покинули игру")
	gb.registeredPlayerInputChannels[userId] <- "sys-deregister"
	close(gb.registeredPlayers[userId].GetOutput())
	delete(gb.registeredPlayers, userId)
	delete(gb.registeredPlayerIds, name)
	delete(gb.registeredPlayerChatIds, userId)
	delete(gb.registeredPlayerInputChannels, userId)

	gb.game.GetWorld().RemovePlayer(name)

	return "Пользователь " + name + " покинул игру"
}

func (gb *GameBot) registerPlayer(name string, userId int, chatId int64) {
	gb.game.AddPlayer(name, gb.commands)
	gb.registeredPlayers[userId] = gb.game.GetWorld().GetPlayer(name)
	gb.registeredPlayerIds[name] = userId
	gb.registeredPlayerChatIds[userId] = chatId
	gb.registeredPlayerInputChannels[userId] = make(chan string)

	go func(output chan string, chatId int64) {
		for data := range output {
			gb.SendMessage(chatId, data)
		}
	}(gb.game.GetWorld().GetPlayer(name).GetOutput(), chatId)

	go func(input chan string) {
	L:
		for {
			select {
			case cmd := <-input:
				if cmd == "sys-deregister" {
					break L
				}
			case <-time.After(time.Second * 15):
				go gb.deregisterPlayerByName(name)
				<-input
				break L
				// the read from ch has timed out
			}
		}
		close(input)
	}(gb.registeredPlayerInputChannels[userId])

}

func (gb *GameBot) reinitGame() {
	if gb.registeredPlayerIds != nil {
		for name := range gb.registeredPlayerIds {
			gb.deregisterPlayerByName(name)
		}
	}

	if gb.game != nil {
		gb.game.Destroy()
	}
	gb.game = game.NewGame()
	gb.game.InitGame()
	gb.commands = game.NewPlayerCommands()

	gb.registeredPlayers = map[int]*game.Player{}
	gb.registeredPlayerIds = map[string]int{}
	gb.registeredPlayerChatIds = map[int]int64{}
	gb.registeredPlayerInputChannels = map[int](chan string){}
}

func NewBot(webhookURL string, port string, apiKey string) *GameBot {
	result := &GameBot{}

	result.Init(webhookURL, port, apiKey)
	result.reinitGame()

	return result
}

func main() {
	port := os.Getenv("PORT")
	apiKey := os.Getenv("TGBOT_API_KEY")
	botOwner, err := strconv.ParseInt(os.Getenv("TGBOT_OWNER"), 10, 64)

	if err != nil {
		panic(err)
	}

	tgbot := NewBot("https://go-game-4.herokuapp.com/", port, apiKey)

	for cmd, data := range tgbot.commands {
		tgbot.RegisterCommand(cmd, func(cmd string, data game.PlayerCommand) func(update *tgbotapi.Update) string {
			return func(update *tgbotapi.Update) string {
				if tgbot.registeredPlayers[update.Message.From.ID] == nil {
					return "Сначала зарегистрируйтесь"
				}

				// Send message to avoid timeout kick
				tgbot.registeredPlayerInputChannels[update.Message.From.ID] <- cmd

				argString := update.Message.CommandArguments()
				args := strings.Fields(argString)

				result := tgbot.registeredPlayers[update.Message.From.ID].DoCommand(cmd, args...)

				log.Println("Executed command with result", result)

				return result
			}
		}(cmd, data))
	}

	tgbot.RegisterCommand("выход", func(update *tgbotapi.Update) string {
		return tgbot.deregisterByUserId(update.Message.From.ID)
	})

	tgbot.RegisterCommand("регистрация", func(update *tgbotapi.Update) string {
		argString := update.Message.CommandArguments()
		args := strings.Split(argString, " ")

		if len(args) != 1 || args[0] == "" {
			return "Ожидалось: /регистрация ВАШ_ЛОГИН"
		}

		if tgbot.registeredPlayers[update.Message.From.ID] != nil {
			return "Вы уже в игре"
		}

		if tgbot.game.GetWorld().HasPlayer(args[0]) {
			return "Такой никнейм уже используется"
		}

		tgbot.registerPlayer(args[0], update.Message.From.ID, update.Message.Chat.ID)

		log.Println("User ID", update.Message.From.ID, "registered as", args[0])

		return "Вы в игре, можете начать с команды /помощь"
	})

	tgbot.RegisterCommand("список_игроков", func(update *tgbotapi.Update) (result string) {
		for name := range tgbot.registeredPlayerIds {
			result += name + " "
		}

		if result == "" {
			result = "Нет игроков"
		}

		return
	})

	tgbot.RegisterCommand("удалить_игрока", func(update *tgbotapi.Update) string {
		if int64(update.Message.From.ID) != botOwner {
			return "Хм... кто здесь?"
		}

		argString := update.Message.CommandArguments()
		args := strings.Split(argString, " ")

		if len(args) != 1 || args[0] == "" {
			return "Ожидалось: /удалить_игрока ИМЯ"
		}

		return tgbot.deregisterPlayerByName(args[0])
	})

	tgbot.RegisterCommand("сброс", func(update *tgbotapi.Update) string {
		if int64(update.Message.From.ID) != botOwner {
			return "Хм... кто здесь?"
		}

		tgbot.reinitGame()

		return "Игра перезагружена"
	})

	go tgbot.Run()

	waitExitSignal()
}
