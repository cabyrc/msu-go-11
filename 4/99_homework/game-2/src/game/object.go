package game

type Inventory map[string]*Object

func (inv *Inventory) HasObject(name string) bool {
	_, ok := (*inv)[name]

	return ok
}

type Object struct {
	name            string
	defaultLocation *ObjectLocation
}

func NewObject(name string, defaultLocation *ObjectLocation) *Object {
	return &Object{name: name, defaultLocation: defaultLocation}
}
