package game

import (
	"strings"
)

type Game struct {
	world *World
}

func (g *Game) addRooms() {
	g.world.AddRoom(NewRoom("кухня", "кухня, ничего интересного.", g.world.eventMap)).GetRoom("кухня").
		SetProperty("описание", "ты находишься на кухне, на столе чай, надо собрать рюкзак и идти в универ.")

	g.world.AddRoom(NewRoom("коридор", "ничего интересного.", g.world.eventMap)).GetRoom("коридор").
		SetProperty("описание", "ты находишься в коридоре, дверь закрыта.").
		SetProperty("дверь", "закрыта")

	g.world.AddRoom(NewRoom("комната", "ты в своей комнате.", g.world.eventMap)).GetRoom("комната").
		SetProperty("описание", "на столе: ключи, конспекты, на стуле - рюкзак.")

	g.world.AddRoom(NewRoom("улица", "на улице весна.", g.world.eventMap)).GetRoom("улица").
		SetProperty("описание", "ты находишься на улице.")
}

func (g *Game) connectRooms() {
	g.world.GetRoom("коридор").ConnectTo(g.world.GetRoom("кухня"), "кухня")
	g.world.GetRoom("кухня").ConnectTo(g.world.GetRoom("коридор"), "коридор")

	g.world.GetRoom("коридор").ConnectTo(g.world.GetRoom("комната"), "комната")
	g.world.GetRoom("комната").ConnectTo(g.world.GetRoom("коридор"), "коридор")

	g.world.GetRoom("коридор").ConnectTo(g.world.GetRoom("улица"), "улица")
	g.world.GetRoom("улица").ConnectTo(g.world.GetRoom("коридор"), "домой")
}

func (g *Game) initRoomEvents() {
	g.world.eventMap.BindEvent("возврат:рюкзак",
		func(player *Player, context interface{}, args ...string) (result interface{}) {
			g.world.GetRoom("кухня").SetProperty("описание", "ты находишься на кухне, на столе чай, надо собрать рюкзак и идти в универ.")

			return context
		})

	g.world.eventMap.BindEvent("возврат:предметы",
		func(player *Player, context interface{}, args ...string) (result interface{}) {
			var descriptions = []string{}
			var description = ""

			for _, object := range *player.inventory {
				object.defaultLocation.AddObject(object)
			}

			room := g.world.GetRoom("комната")

			for _, name := range room.locationNames {
				if len(room.locations[name].objectNames) > 0 {
					descriptions = append(descriptions, name+" "+strings.Join(room.locations[name].objectNames, ", "))
				}
			}

			if len(descriptions) > 0 {
				description = strings.Join(descriptions, ", ") + "."
			} else {
				description = "пустая комната."
			}

			room.SetProperty("описание", description)

			return context
		})

	g.world.eventMap.BindEvent("выполнено:собрать_рюкзак",
		func(player *Player, context interface{}, args ...string) (result interface{}) {
			g.world.GetRoom("кухня").SetProperty("описание", "ты находишься на кухне, на столе чай, надо идти в универ.")

			return context
		})

	g.world.GetRoom("комната").BindEventHandler("команда:взять",
		func(player *Player, context interface{}, args ...string) (result interface{}) {
			var descriptions = []string{}
			var description = ""

			for _, name := range player.room.locationNames {
				if len(player.room.locations[name].objectNames) > 0 {
					descriptions = append(descriptions, name+" "+strings.Join(player.room.locations[name].objectNames, ", "))
				}
			}

			if len(descriptions) > 0 {
				description = strings.Join(descriptions, ", ") + "."
			} else {
				description = "пустая комната."
			}

			player.room.SetProperty("описание", description)

			if args[0] == "конспекты" {
				player.commands.events.SendEvent("выполнено:собрать_рюкзак", player, "")
			}

			return context
		})

	g.world.GetRoom("комната").BindEventHandler("pre:команда:надеть",
		func(player *Player, context interface{}, args ...string) (result interface{}) {
			if args[0] == "рюкзак" && player.inventory == nil {
				var descriptions = []string{}

				for _, name := range player.room.locationNames {
					descriptions = append(descriptions, name+" "+strings.Join(player.room.locations[name].objectNames, ", "))
				}

				var description = strings.Join(descriptions, ", ") + "."

				player.room.SetProperty("описание", description)

				player.inventory = &Inventory{}
				return "вы надели: рюкзак"
			}
			return context
		})

	g.world.GetRoom("коридор").BindEventHandler("pre:команда:идти",
		func(player *Player, context interface{}, args ...string) (result interface{}) {
			if args[0] == "улица" {
				value, ok := player.room.GetProperty("дверь")

				if ok && value == "закрыта" {
					return "дверь закрыта"
				}
			}
			return context
		})

	g.world.GetRoom("коридор").BindEventHandler("pre:команда:применить",
		func(player *Player, context interface{}, args ...string) (result interface{}) {
			if player.inventory == nil || !player.inventory.HasObject(args[0]) {
				return context
			}

			if args[0] == "ключи" && args[1] == "дверь" {
				value, ok := player.room.GetProperty("дверь")

				if ok && value == "закрыта" {
					player.room.SetProperty("описание", "ты находишься в коридоре, дверь открыта.")
					player.room.SetProperty("дверь", "открыта")
					return "дверь открыта"
				}

				if ok && value == "открыта" {
					player.room.SetProperty("описание", "ты находишься в коридоре, дверь закрыта.")
					player.room.SetProperty("дверь", "закрыта")
					return "дверь закрыта"
				}

			}
			return context
		})
}

func (g *Game) initRoomLocations() {
	room := g.world.GetRoom("комната")
	var location = room.AddObjectLocation("на столе:")
	location.AddObject(NewObject("ключи", location))
	location.AddObject(NewObject("конспекты", location))
}

func (g *Game) initWorld() {
	g.addRooms()
	g.connectRooms()
	g.initRoomLocations()
}

func (g *Game) GetWorld() *World {
	return g.world
}

func (g *Game) AddPlayer(name string, commands map[string]PlayerCommand) {
	NewPlayer(name, g.world, commands)
}

func (g *Game) InitGame() {
	g.initWorld()
	g.initRoomEvents()
}

func (g *Game) Destroy() {
	g.world.Destroy()
}

func NewGame() (g *Game) {
	return &Game{
		world: NewWorld(),
	}
}

/*
func main() {
	initGame()
	players := make([]*Player, 2)
	players[0] = NewPlayer("Tristan")
	addPlayer(players[0])
	players[1] = NewPlayer("вася")
	addPlayer(players[1])

	var command string

	var reader *bufio.Reader = bufio.NewReader(os.Stdin)

	for {
		select {
		case s := <-players[0].output:
			fmt.Printf("[%s] %s\n", players[0].name, s)
		case s := <-players[1].output:
			fmt.Printf("[%s] %s\n", players[1].name, s)
		default:
			fmt.Print("Введите команду: ")

			command, _ = reader.ReadString('\n')
			command = strings.TrimRight(command, "\n\r ")

			if command == "выход" {
				return
			}

			players[0].HandleInput(command)
			runtime.Gosched()
		}
	}
}
*/
