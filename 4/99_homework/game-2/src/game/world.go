package game

type World struct {
	destroyed bool
	eventMap  *EventMap
	rooms     map[string]*Room
	players   map[string]*Player
}

func (w *World) AddRoom(r *Room) *World {
	if w.HasRoom(r.name) {
		panic("Room " + r.name + " already exists")
	}
	w.rooms[r.name] = r

	return w
}

func (w *World) HasRoom(name string) bool {
	_, ok := w.rooms[name]

	return ok
}

func (w *World) GetRoom(name string) *Room {
	if !w.HasRoom(name) {
		panic("Room " + name + " does not exist")
	}

	return w.rooms[name]
}

func (w *World) HasPlayer(name string) bool {
	_, ok := w.players[name]

	return ok
}

func (w *World) GetPlayer(name string) *Player {
	if !w.HasPlayer(name) {
		panic("Player " + name + " does not exist")
	}

	return w.players[name]
}

func (w *World) AddPlayer(player *Player) *World {
	if w.HasPlayer(player.name) {
		panic("Player " + player.name + " already exists")
	}

	w.players[player.name] = player
	player.world = w
	player.room = w.GetRoom("кухня")
	player.room.playerIn <- player

	return w
}

func (w *World) RemovePlayer(name string) *World {
	if !w.HasPlayer(name) {
		panic("Player " + name + " does not exist")
	}

	player := w.GetPlayer(name)
	player.room.playerOut <- player
	delete(w.players, player.name)

	// Player doesn't have anything, so nothing to return
	if player.inventory == nil {
		return w
	}

	w.eventMap.SendEvent("возврат:предметы", player, "RemovePlayer")
	w.eventMap.SendEvent("возврат:рюкзак", player, "RemovePlayer")

	return w
}

func (w *World) GetPlayers() map[string]*Player {
	return w.players
}

func (w *World) Destroy() {
	for _, player := range w.players {
		player.room.playerOut <- player
		delete(w.players, player.name)
	}

	for _, room := range w.rooms {
		room.destroyed = true
		delete(w.rooms, room.name)
	}

	w.destroyed = true
}

func NewWorld() *World {
	return &World{
		eventMap: NewEventMap(),
		players:  make(map[string]*Player),
		rooms:    make(map[string]*Room),
	}
}
