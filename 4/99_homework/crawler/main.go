package main

import (
	"golang.org/x/net/html"
	"io"
	"log"
	"net/http"
	"net/url"
	"strings"
)

type UrlData struct {
	baseUrl string
	paths   []string
}

type Urls struct {
	CrawledMap   map[string]bool
	CrawledSlice []string
}

type Crawler struct {
	Urls    *Urls
	BaseUrl string
}

func (c *Crawler) Crawl(host string) *Crawler {
	parsedUrl := c.parseUrl(host)

	if _, ok := c.Urls.CrawledMap[parsedUrl.Path]; ok {
		return c
	}
	log.Println("Crawling " + host)

	resp := c.getHttpResponse(host)
	defer resp.Body.Close()

	if resp.StatusCode == 301 {
		c.Crawl(host + resp.Header.Get("Location"))
		return c
	}

	if resp.StatusCode != 200 {
		log.Printf("Status code is %d\n", resp.StatusCode)
		return c
	}

	c.savePath(parsedUrl.Path)

	paths := c.parseBody(resp.Body)

	for _, val := range paths {
		parsedUrl := c.parseUrl(val)
		if parsedUrl.Host == "" {
			c.Crawl(c.BaseUrl + val)
		}
	}

	return c
}

func (c *Crawler) parseNode(n *html.Node, urlData *UrlData) {
	if n.Type == html.ElementNode && n.Data == "base" {
		for _, val := range n.Attr {
			if val.Key == "href" {
				urlData.baseUrl = val.Val
				break
			}
		}
	} else if n.Type == html.ElementNode && n.Data == "a" {
		for _, val := range n.Attr {
			if val.Key == "href" {
				urlData.paths = append(urlData.paths, val.Val)
			}
		}
	}
	for ch := n.FirstChild; ch != nil; ch = ch.NextSibling {
		c.parseNode(ch, urlData)
	}
}

func (c *Crawler) parseBody(body io.ReadCloser) []string {
	doc, err := html.Parse(body)
	if err != nil {
		log.Println("Error parsing html:", err)
		return []string{}
	}

	urlData := &UrlData{
		baseUrl: "",
		paths:   []string{},
	}

	c.parseNode(doc, urlData)

	if urlData.baseUrl != "" {
		for i, val := range urlData.paths {
			if string([]rune(val)[0]) != "/" {
				urlData.paths[i] = urlData.baseUrl + urlData.paths[i]
			}
		}
	}

	return urlData.paths
}

func (c *Crawler) savePath(path string) {
	c.Urls.CrawledMap[path] = true
	c.Urls.CrawledSlice = append(c.Urls.CrawledSlice, path)
}

func (c *Crawler) getHttpResponse(host string) *http.Response {
	cl := http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	resp, err := cl.Get(host)

	if err != nil {
		log.Fatal(err)
	}

	return resp
}

func (c *Crawler) parseUrl(uri string) *url.URL {
	u, err := url.Parse(uri)
	if err != nil {
		log.Fatal(err)
	}

	return u
}

func Crawl(host string) []string {
	crawler := &Crawler{
		Urls: &Urls{
			map[string]bool{},
			[]string{},
		},
	}

	urls := strings.Split(host, "?")
	u := crawler.parseUrl(urls[0])
	crawler.BaseUrl = host[:len(host)-len(u.Path)]

	crawler.Crawl(host)

	return crawler.Urls.CrawledSlice
}

func main() {

}
