package main

import (
	"encoding/json"
	"github.com/garyburd/redigo/redis"
	"log"
	"time"
)

//PanicOnErr panics on error
func PanicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}

var (
	conn redis.Conn
)

type CacheItem struct {
	Data interface{} // можем класть любые данные
	Tags map[string]int
}

type Articles []Article

type Article struct {
	Name string
	From string
}

func getCacheRecord(mkey string) (string, error) {
	log.Println("getting cache record by key \"" + mkey + "\"")

	// получает запись, https://redis.io/commands/get
	item, err := redis.String(conn.Do("GET", mkey))
	// если записи нету, то для этого есть специальная ошибка, её надо обрабатывать отдеьно, это почти штатная ситуация, а не что-то страшное
	if err == redis.ErrNil {
		log.Println("Record not found in redis (return value is nil)")
		return "", err
	} else if err != nil {
		PanicOnErr(err)
	}
	return item, nil
}

func saveCacheRecord(mkey string, item interface{}) {
	log.Println("saving item to cache by key \"" + mkey + "\"")

	data, _ := json.Marshal(item)

	// получает запись, https://redis.io/commands/get
	item, err := redis.String(conn.Do("SET", mkey, data))
	// если записи нету, то для этого есть специальная ошибка, её надо обрабатывать отдеьно, это почти штатная ситуация, а не что-то страшное
	if err != nil {
		PanicOnErr(err)
	}
}

func TcacheGet(mkey string, buildCache func() (interface{}, []string)) (result CacheItem) {
	var data string
	var err error

	for i := 0; i < 10000; i++ {
		data, err = getCacheRecord(mkey)

		if err == redis.ErrNil {
			log.Println("setting lock", mkey+"_lock")
			lockStatus, _ := redis.String(conn.Do("SET", mkey+"_lock", "locked", "NX"))
			if lockStatus != "OK" {
				// кто-то другой держит лок, подождём и попробуем получить запись еще раз
				log.Println("Cannot lock (someone is already building cache), sleep 1s then try again", i)
				time.Sleep(time.Second)
			} else {
				log.Println("Lock acquired, going to build cache", i)
				newData, tags := buildCache()

				cItem := CacheItem{}
				cItem.Data = newData
				cItem.Tags = map[string]int{}

				for _, tag := range tags {
					n, err := redis.Int(conn.Do("INCR", tag))
					if err != nil {
						PanicOnErr(err)
					}
					cItem.Tags[tag] = n
				}

				saveCacheRecord(mkey, cItem)

				log.Println("deleting the lock")
				n, err := redis.Int(conn.Do("DEL", mkey+"_lock"))
				PanicOnErr(err)
				log.Println("lock deleted:", n)

				return cItem
			}
		} else {
			break
		}
	}

	if err == redis.ErrNil {
		panic("Couldn't get cache record - locked")
	}

	cItems := CacheItem{}
	err = json.Unmarshal([]byte(data), &cItems)

	if err != nil {
		PanicOnErr(err)
	}

	return cItems
}

func main() {
	var err error
	conn, err = redis.DialURL("redis://localhost:6379/0")

	//conn, err := redis.DialURL(os.Getenv("REDIS_URL"))
	PanicOnErr(err)
	defer conn.Close()

	var data = TcacheGet("test", func() (interface{}, []string) {
		log.Println("Simulating 25s cache build procedure")
		// simulate cache build
		time.Sleep(time.Second * 25)

		return "test", []string{"fest", "best"}
	})

	log.Println("data got", data)
}
