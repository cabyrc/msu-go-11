package main

import "sync"

type RoundRobinBalancer struct {
	sync.Mutex
	maxServers int
	nextServer int
	stats      []int
}

func (r *RoundRobinBalancer) Init(i int) {
	r.maxServers = i
	r.stats = make([]int, i)
}

func (r *RoundRobinBalancer) GiveStat() []int {
	return r.stats
}

func (r *RoundRobinBalancer) GiveNode() int {
	r.Lock()
	defer r.Unlock()

	r.stats[r.nextServer] += 1

	r.nextServer += 1

	if r.nextServer == r.maxServers {
		r.nextServer = 0
	}

	return r.nextServer
}
