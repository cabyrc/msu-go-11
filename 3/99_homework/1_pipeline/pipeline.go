package pipeline

import (
	"sync"
)

type job func(in, out chan interface{})

func makeJob(wg *sync.WaitGroup, fn job, in chan interface{}) (out chan interface{}) {
	out = make(chan interface{})

	wg.Add(1)
	go func() {
		fn(in, out)
		close(out)
		wg.Done()
	}()

	return out
}

func Pipe(funcs ...job) {
	var out chan interface{}
	wg := sync.WaitGroup{}

	for _, fn := range funcs {
		out = makeJob(&wg, fn, out)
	}

	wg.Wait()

	return
}
