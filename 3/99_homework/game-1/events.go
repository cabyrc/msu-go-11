package main

type EventCallback func(player *Player, context interface{}, args ...string) (result interface{})
type EventMap map[string][]EventCallback

var events *EventMap

func (em *EventMap) BindEvent(name string, callback EventCallback) {
	if !em.HasEvent(name) {
		(*em)[name] = []EventCallback{callback}
	} else {
		(*em)[name] = append((*em)[name], callback)
	}
}

func (em *EventMap) HasEvent(name string) bool {
	_, ok := (*em)[name]

	return ok
}

func (em *EventMap) SendEvent(name string, player *Player, context interface{}, args ...string) (result interface{}) {
	result = context

	if !em.HasEvent(name) {
		return
	}

	for _, callback := range (*em)[name] {
		result = callback(player, context, args...)
	}

	return
}

func GetEventMapInstance() *EventMap {
	if events == nil {
		events = &EventMap{}
	}

	return events
}
