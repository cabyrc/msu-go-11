package main

import (
	"bufio"
	"fmt"
	"os"
	"runtime"
	"strings"
)

func addRooms() {
	GetWorldInstance().AddRoom(NewRoom("кухня", "кухня, ничего интересного.", GetEventMapInstance())).GetRoom("кухня").
		SetProperty("описание", "ты находишься на кухне, на столе чай, надо собрать рюкзак и идти в универ.")

	GetWorldInstance().AddRoom(NewRoom("коридор", "ничего интересного.", GetEventMapInstance())).GetRoom("коридор").
		SetProperty("описание", "ты находишься в коридоре, дверь закрыта.").
		SetProperty("дверь", "закрыта")

	GetWorldInstance().AddRoom(NewRoom("комната", "ты в своей комнате.", GetEventMapInstance())).GetRoom("комната").
		SetProperty("описание", "на столе: ключи, конспекты, на стуле - рюкзак.")

	GetWorldInstance().AddRoom(NewRoom("улица", "на улице весна.", GetEventMapInstance())).GetRoom("улица").
		SetProperty("описание", "ты находишься на улице.")
}

func connectRooms() {
	GetWorldInstance().GetRoom("коридор").ConnectTo(GetWorldInstance().GetRoom("кухня"), "кухня")
	GetWorldInstance().GetRoom("кухня").ConnectTo(GetWorldInstance().GetRoom("коридор"), "коридор")

	GetWorldInstance().GetRoom("коридор").ConnectTo(GetWorldInstance().GetRoom("комната"), "комната")
	GetWorldInstance().GetRoom("комната").ConnectTo(GetWorldInstance().GetRoom("коридор"), "коридор")

	GetWorldInstance().GetRoom("коридор").ConnectTo(GetWorldInstance().GetRoom("улица"), "улица")
	GetWorldInstance().GetRoom("улица").ConnectTo(GetWorldInstance().GetRoom("коридор"), "домой")
}

func initRoomEvents() {
	GetEventMapInstance().BindEvent("выполнено:собрать_рюкзак",
		func(player *Player, context interface{}, args ...string) (result interface{}) {
			GetWorldInstance().GetRoom("кухня").SetProperty("описание", "ты находишься на кухне, на столе чай, надо идти в универ.")

			return context
		})

	GetWorldInstance().GetRoom("комната").BindEventHandler("команда:взять",
		func(player *Player, context interface{}, args ...string) (result interface{}) {
			var descriptions = []string{}
			var description = ""

			for _, name := range player.room.locationNames {
				if len(player.room.locations[name].objectNames) > 0 {
					descriptions = append(descriptions, name+" "+strings.Join(player.room.locations[name].objectNames, ", "))
				}
			}

			if len(descriptions) > 0 {
				description = strings.Join(descriptions, ", ") + "."
			} else {
				description = "пустая комната."
			}

			player.room.SetProperty("описание", description)

			if args[0] == "конспекты" {
				player.commands.events.SendEvent("выполнено:собрать_рюкзак", player, "")
			}

			return context
		})

	GetWorldInstance().GetRoom("комната").BindEventHandler("pre:команда:надеть",
		func(player *Player, context interface{}, args ...string) (result interface{}) {
			if args[0] == "рюкзак" && player.inventory == nil {
				var descriptions = []string{}

				for _, name := range player.room.locationNames {
					descriptions = append(descriptions, name+" "+strings.Join(player.room.locations[name].objectNames, ", "))
				}

				var description = strings.Join(descriptions, ", ") + "."

				player.room.SetProperty("описание", description)

				player.inventory = &Inventory{}
				return "вы надели: рюкзак"
			}
			return context
		})

	GetWorldInstance().GetRoom("коридор").BindEventHandler("pre:команда:идти",
		func(player *Player, context interface{}, args ...string) (result interface{}) {
			if args[0] == "улица" {
				value, ok := player.room.GetProperty("дверь")

				if ok && value == "закрыта" {
					return "дверь закрыта"
				}
			}
			return context
		})

	GetWorldInstance().GetRoom("коридор").BindEventHandler("pre:команда:применить",
		func(player *Player, context interface{}, args ...string) (result interface{}) {
			if player.inventory == nil || !player.inventory.hasObject(args[0]) {
				return context
			}

			if args[0] == "ключи" && args[1] == "дверь" {
				value, ok := player.room.GetProperty("дверь")

				if ok && value == "закрыта" {
					player.room.SetProperty("описание", "ты находишься в коридоре, дверь открыта.")
					player.room.SetProperty("дверь", "открыта")
					return "дверь открыта"
				}

				if ok && value == "открыта" {
					player.room.SetProperty("описание", "ты находишься в коридоре, дверь закрыта.")
					player.room.SetProperty("дверь", "закрыта")
					return "дверь закрыта"
				}

			}
			return context
		})
}

func initRoomLocations() {
	room := GetWorldInstance().GetRoom("комната")
	var location = room.AddObjectLocation("на столе:")
	location.AddObject(NewObject("ключи"))
	location.AddObject(NewObject("конспекты"))
}

func initWorld() {
	world = nil
	addRooms()
	connectRooms()
	initRoomLocations()
}

func addPlayer(player *Player) {
	GetWorldInstance().AddPlayer(player)
}

func initGame() {
	initWorld()
	initRoomEvents()
}
func fanIn(input1, input2 <-chan string) <-chan string {
	c := make(chan string)
	go func() {
		for {
			select {
			case s := <-input1:
				fmt.Printf("[%s] %s\n", "p1", s)
				c <- s
			case s := <-input2:
				fmt.Printf("[%s] %s\n", "p2", s)
				c <- s
			}
		}
	}()
	return c
}
func main() {
	initGame()
	players := make([]*Player, 2)
	players[0] = NewPlayer("Tristan")
	addPlayer(players[0])
	players[1] = NewPlayer("вася")
	addPlayer(players[1])

	var command string

	var reader *bufio.Reader = bufio.NewReader(os.Stdin)

	for {
		select {
		case s := <-players[0].output:
			fmt.Printf("[%s] %s\n", players[0].name, s)
		case s := <-players[1].output:
			fmt.Printf("[%s] %s\n", players[1].name, s)
		default:
			fmt.Print("Введите команду: ")

			command, _ = reader.ReadString('\n')
			command = strings.TrimRight(command, "\n\r ")

			if command == "выход" {
				return
			}

			players[0].HandleInput(command)
			runtime.Gosched()
		}
	}

}
