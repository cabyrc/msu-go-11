package main

// World is actually a map of rooms
type World struct {
	rooms   map[string]*Room
	players map[string]*Player
}

var world *World

func (w *World) AddRoom(r *Room) *World {
	if w.HasRoom(r.name) {
		panic("Room " + r.name + " already exists")
	}
	w.rooms[r.name] = r

	return w
}

func (w *World) HasRoom(name string) bool {
	_, ok := w.rooms[name]

	return ok
}

func (w *World) GetRoom(name string) *Room {
	if !w.HasRoom(name) {
		panic("Room " + name + " does not exist")
	}

	return w.rooms[name]
}

func (w *World) HasPlayer(name string) bool {
	_, ok := w.players[name]

	return ok
}

func (w *World) GetPlayer(name string) *Player {
	if !w.HasPlayer(name) {
		panic("Player " + name + " does not exist")
	}

	return w.players[name]
}

func (w *World) AddPlayer(player *Player) *World {
	if w.HasPlayer(player.name) {
		panic("Player " + player.name + " already exists")
	}

	w.players[player.name] = player
	player.world = w
	player.room = world.GetRoom("кухня")
	player.room.playerIn <- player

	return w
}

func (w *World) GetPlayers() map[string]*Player {
	return w.players
}

func GetWorldInstance() *World {
	if world == nil {
		world = &World{
			players: make(map[string]*Player),
			rooms:   make(map[string]*Room),
		}
	}

	return world
}
