package main

type Inventory map[string]*Object

func (inv *Inventory) hasObject(name string) bool {
	_, ok := (*inv)[name]

	return ok
}

type Object struct {
	name string
}

func NewObject(name string) *Object {
	return &Object{name: name}
}
