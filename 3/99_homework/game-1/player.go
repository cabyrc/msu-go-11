package main

import (
	"fmt"
	"strings"
)

type Player struct {
	name      string
	world     *World
	room      *Room
	inventory *Inventory
	commands  *CommandMap
	output    chan string
}

type playerCommand struct {
	argLengthMin int
	argLengthMax int
	handler      func(player *Player, args ...string) (result string, success bool)
}

func newPlayerCommands() map[string]playerCommand {
	var playerCommands map[string]playerCommand

	playerCommands = map[string]playerCommand{
		"инвентарь": {
			argLengthMin: 0,
			argLengthMax: 0,
			handler: func(player *Player, args ...string) (result string, success bool) {
				if player.inventory == nil {
					return "место для инвентаря отсутствует", false
				}

				var objects = []string{}

				for key := range *player.inventory {
					objects = append(objects, key)
				}

				result = strings.Join(objects, ", ")

				if result == "" {
					result = "инвентарь пуст"
				}

				return result, true
			},
		},
		"осмотреться": {
			argLengthMin: 0,
			argLengthMax: 0,
			handler: func(player *Player, args ...string) (result string, success bool) {
				msg := player.room.GetLookAroundMsg()

				users := []string{}
				for p := range player.room.GetPlayers() {
					if p.name != player.name {
						users = append(users, p.name)
					}
				}

				pcount := len(users)
				if pcount > 0 {
					msg += ". Кроме вас тут ещё "
					msg += strings.Join(users, ", ")
				}

				return msg, true
			},
		},
		"идти": {
			argLengthMin: 1,
			argLengthMax: 1,
			handler: func(player *Player, args ...string) (result string, success bool) {
				if !player.GoToRoom(args[0]) {
					return "нет пути в " + args[0], false
				}
				return player.room.GetInRoomMsg(), true
			},
		},
		"надеть": {
			argLengthMin: 1,
			argLengthMax: 1,
			handler: func(player *Player, args ...string) (result string, success bool) {
				return "нет такого", false
			},
		},
		"взять": {
			argLengthMin: 1,
			argLengthMax: 1,
			handler: func(player *Player, args ...string) (result string, success bool) {
				if player.inventory == nil {
					return "некуда класть", false
				}

				result = "нет такого"
				success = false

				var object = player.PickObject(args[0])

				if object != nil {
					result = "предмет добавлен в инвентарь: " + object.name
					success = true
				}

				return
			},
		},
		"применить": {
			argLengthMin: 2,
			argLengthMax: 2,
			handler: func(player *Player, args ...string) (result string, success bool) {
				if player.inventory == nil || !player.inventory.hasObject(args[0]) {
					return "нет предмета в инвентаре - " + args[0], false
				}

				return "не к чему применить", false
			},
		},
		"сказать": {
			argLengthMin: 1,
			argLengthMax: 1000,
			handler: func(player *Player, args ...string) (result string, success bool) {
				msg := strings.Join(args, " ")
				for _, p := range player.world.GetPlayers() {
					if p.name != player.name {
						go p.AddOutput(fmt.Sprintf("%s говорит: %s", player.name, msg))
					}
				}

				return fmt.Sprintf("%s говорит: %s", player.name, msg), true
			},
		},
		"сказать_игроку": {
			argLengthMin: 1,
			argLengthMax: 1000,
			handler: func(player *Player, args ...string) (result string, success bool) {
				if !player.world.HasPlayer(args[0]) {
					//return "нет игрока - " + args[0], false
					return "тут нет такого игрока", false
				}

				msg := strings.Join(args[1:], " ")

				toPlayer := player.world.GetPlayer(args[0])

				if toPlayer.room.name != player.room.name {
					return "тут нет такого игрока", false
				}

				if msg != "" {
					go toPlayer.AddOutput(fmt.Sprintf("%s говорит вам: %s", player.name, msg))
				} else {
					go toPlayer.AddOutput(fmt.Sprintf("%s выразительно молчит, смотря на вас", player.name))
				}

				//return fmt.Sprintf("Вы сказали игроку %s: %s", toPlayer.name, msg), true
				return "", true
			},
		},
		"где_игрок": {
			argLengthMin: 1,
			argLengthMax: 1,
			handler: func(player *Player, args ...string) (result string, success bool) {
				if !player.world.HasPlayer(args[0]) {
					//return "нет игрока - " + args[0], false
					return "тут нет такого игрока", false
				}

				p := player.world.GetPlayer(args[0])

				return fmt.Sprintf("Игрок %s в %s", p.name, p.room.name), true
			},
		},
		"выход": {
			argLengthMin: 0,
			argLengthMax: 0,
			handler:      func(player *Player, args ...string) (result string, success bool) { return "", false },
		},
		"помощь": {
			argLengthMin: 0,
			argLengthMax: 0,
			handler: func(player *Player, args ...string) (result string, success bool) {
				keys := make([]string, len(playerCommands))

				i := 0
				for k := range playerCommands {
					keys[i] = k
					i++
				}

				return "Доступные команты: " + strings.Join(keys, ", "), true
			},
		},
	}

	return playerCommands
}

func NewPlayer(name string) (player *Player) {
	player = &Player{
		name:     name,
		commands: GetCommandMapInstance(),
		output:   make(chan string),
	}

	for cmd, data := range newPlayerCommands() {
		player.commands.AddCommand(cmd, data.argLengthMin, data.argLengthMax, data.handler)
	}

	return
}

func (p *Player) CanGoToRoom(name string) bool {
	if !p.room.HasConnectedRoom(name) {
		return false
	}

	return true
}

func (p *Player) GoToRoom(name string) bool {
	if !p.CanGoToRoom(name) {
		return false
	}
	p.room.playerOut <- p
	p.room = p.room.GetConnectedRoom(name)
	p.room.playerIn <- p

	return true
}

func (p *Player) CanPickObject(name string) bool {
	if !p.room.HasObject(name) {
		return false
	}

	return true
}

func (p *Player) PickObject(name string) *Object {
	if p.inventory == nil {
		return nil
	}

	if !p.CanPickObject(name) {
		return nil
	}

	var object = p.room.PickObject(name)

	if object == nil {
		return nil
	}

	(*p.inventory)[name] = object

	return object
}

func (p *Player) AddOutput(msg string) {
	p.output <- msg
}

func (p *Player) GetOutput() (result chan string) {
	return p.output
}

// Handles command
func (p *Player) HandleInput(input string) (result string) {
	command := strings.Fields(input)

	if len(command) == 0 {
		return "команда не задана"
	}
	result = p.commands.DoCommand(command[0], p, command[1:]...)

	if result != "" {
		go p.AddOutput(result)
	}

	return
}
