package main

import (
	"fmt"
)

func Abs(val float64) (result float64) {
    result = val

    if val < 0 {
        result = -result
    }

    return
}

// вычисление Квадратного корня
func Sqrt(x float64) (result float64) {
    if x < 0 {
        return 0 // not totally good, as it's actually NaN, but we are not allowed to use math
    }

    result = x
	epsilon := 0.01 * 0.01
    previous := 0.01

    for Abs(result * result - x) > epsilon {
        previous = result
    	result = 0.5 * (previous + x/previous)
    }

    return
}

func main() {
    //fmt.Println("Expected", math.Sqrt(25))
	fmt.Println("Result", Sqrt(25))
}
