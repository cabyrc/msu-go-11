package main

import "fmt"
import "strings"
import "bytes"

type memoizeFunction func(int, ...int) interface{}

// TODO реализовать
var fibonacci memoizeFunction
var romanForDecimal memoizeFunction

//TODO Write memoization function
func memoize(function memoizeFunction) memoizeFunction {
	cache := make(map[string]interface{})

    return func(x int, xs ...int) interface{} {
        key := fmt.Sprint(x)
        for _, i := range xs {
            key += fmt.Sprintf(":%d", i)
        }
        if value, found := cache[key]; found {
            return value
        }
        value := function(x, xs...)
        cache[key] = value
        return value
    }
}

// TODO обернуть функции fibonacci и roman в memoize
func init() {
	romanDigits := map[int]string{
        0: "",
        1: "I",
        4: "IV",
        5: "V",
        9: "IX",
        10: "X",
        40: "XL",
        50: "L",
        90: "XC",
        100: "C",
        400: "CD",
        500: "D",
        900: "CM",
        1000: "M",
    }

    fibonacci = memoize(func(x int, xs ...int) interface{} {
        if x < 2 {
            return x
        }

        return fibonacci(x-1).(int) + fibonacci(x-2).(int)
    })


    romanForDecimal = memoize(func(x int, xs ...int) interface{} {
        if x < 0 || x > 3999 {
            panic("RomanForDecimal() only handles integers [0, 3999]")
        }

        i := 1
        cur := 0

        result := ""

        for x > 0 {
            cur = x % 10
            x /= 10

            switch cur {
                case 0:
                    if i != 1 {
                        result = romanDigits[i] + result
                    }
                case 1, 4, 5, 9:
                    result = romanDigits[cur * i] + result
                case 2,3:
                    result = strings.Repeat(romanDigits[i], cur) + result
                case 6,7,8:
                    result = romanDigits[5 * i] + strings.Repeat(romanDigits[i], cur - 5) + result
            }

            i *= 10
        }

        return result
    })

    decimals := []int{1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1}    
    romans := []string{"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"}

    romanForDecimal = memoize(func(x int, xs ...int) interface{} {
        if x < 0 || x > 3999 {
            panic("RomanForDecimal() only handles integers [0, 3999]")        
        }        
        var buffer bytes.Buffer        
        for i, decimal := range decimals {
            remainder := x / decimal            
            x %= decimal            
            if remainder > 0 {                
                buffer.WriteString(strings.Repeat(romans[i], remainder))            
            }
        }
        return buffer.String()
    })
}

func main() {
	fmt.Println("Fibonacci(45) =", fibonacci(45).(int))
	for _, x := range []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
		14, 15, 16, 17, 18, 19, 20, 25, 30, 40, 50, 60, 69, 70, 80,
		90, 99, 100, 200, 300, 400, 500, 600, 666, 700, 800, 900,
		1000, 1009, 1444, 1666, 1945, 1997, 1999, 2000, 2008, 2010,
		2012, 2500, 3000, 3999} {
		fmt.Printf("%4d = %s\n", x, romanForDecimal(x).(string))
	}
}
