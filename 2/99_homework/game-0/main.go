package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

var events EventMap
var commands CommandMap
var player Player
var world World

func addRooms() {
	world.AddRoom(NewRoom("кухня", "кухня, ничего интересного.", &events)).
		SetProperty("описание", "ты находишься на кухне, на столе чай, надо собрать рюкзак и идти в универ.")

	world.AddRoom(NewRoom("коридор", "ничего интересного.", &events)).
		SetProperty("описание", "ты находишься в коридоре, дверь закрыта.").
		SetProperty("дверь", "закрыта")

	world.AddRoom(NewRoom("комната", "ты в своей комнате.", &events)).
		SetProperty("описание", "на столе: ключи, конспекты, на стуле - рюкзак.")

	world.AddRoom(NewRoom("улица", "на улице весна.", &events)).
		SetProperty("описание", "ты находишься на улице.")
}

func connectRooms() {
	world.GetRoom("коридор").ConnectTo(world.GetRoom("кухня"), "кухня")
	world.GetRoom("кухня").ConnectTo(world.GetRoom("коридор"), "коридор")

	world.GetRoom("коридор").ConnectTo(world.GetRoom("комната"), "комната")
	world.GetRoom("комната").ConnectTo(world.GetRoom("коридор"), "коридор")

	world.GetRoom("коридор").ConnectTo(world.GetRoom("улица"), "улица")
	world.GetRoom("улица").ConnectTo(world.GetRoom("коридор"), "домой")
}

func initRoomEvents() {
	events.BindEvent("выполнено:собрать_рюкзак",
		func(player *Player, context interface{}, args ...string) (result interface{}) {
			world.GetRoom("кухня").SetProperty("описание", "ты находишься на кухне, на столе чай, надо идти в универ.")

			return context
		})

	world.GetRoom("комната").BindEventHandler("команда:взять",
		func(player *Player, context interface{}, args ...string) (result interface{}) {
			var descriptions = []string{}
			var description = ""

			for _, name := range player.room.locationNames {
				if len(player.room.locations[name].objectNames) > 0 {
					descriptions = append(descriptions, name+" "+strings.Join(player.room.locations[name].objectNames, ", "))
				}
			}

			if len(descriptions) > 0 {
				description = strings.Join(descriptions, ", ") + "."
			} else {
				description = "пустая комната."
			}

			player.room.SetProperty("описание", description)

			if args[0] == "конспекты" {
				player.commands.events.SendEvent("выполнено:собрать_рюкзак", player, "")
			}

			return context
		})

	world.GetRoom("комната").BindEventHandler("pre:команда:надеть",
		func(player *Player, context interface{}, args ...string) (result interface{}) {
			if args[0] == "рюкзак" && player.inventory == nil {
				var descriptions = []string{}

				for _, name := range player.room.locationNames {
					descriptions = append(descriptions, name+" "+strings.Join(player.room.locations[name].objectNames, ", "))
				}

				var description = strings.Join(descriptions, ", ") + "."

				player.room.SetProperty("описание", description)

				player.inventory = &Inventory{}
				return "вы надели: рюкзак"
			}
			return context
		})

	world.GetRoom("коридор").BindEventHandler("pre:команда:идти",
		func(player *Player, context interface{}, args ...string) (result interface{}) {
			if args[0] == "улица" {
				value, ok := player.room.GetProperty("дверь")

				if ok && value == "закрыта" {
					return "дверь закрыта"
				}
			}
			return context
		})

	world.GetRoom("коридор").BindEventHandler("pre:команда:применить",
		func(player *Player, context interface{}, args ...string) (result interface{}) {
			if player.inventory == nil || !player.inventory.hasObject(args[0]) {
				return context
			}

			if args[0] == "ключи" && args[1] == "дверь" {
				value, ok := player.room.GetProperty("дверь")

				if ok && value == "закрыта" {
					player.room.SetProperty("описание", "ты находишься в коридоре, дверь открыта.")
					player.room.SetProperty("дверь", "открыта")
					return "дверь открыта"
				}

				if ok && value == "открыта" {
					player.room.SetProperty("описание", "ты находишься в коридоре, дверь закрыта.")
					player.room.SetProperty("дверь", "закрыта")
					return "дверь закрыта"
				}

			}
			return context
		})
}

func initRoomLocations() {
	room := world.GetRoom("комната")
	var location = room.AddObjectLocation("на столе:")
	location.AddObject(NewObject("ключи"))
	location.AddObject(NewObject("конспекты"))
}

func initWorld() {
	world = World{}
	events = EventMap{}
	commands = NewCommandMap(&events)

	addRooms()
	connectRooms()
	initRoomLocations()
}

func initPlayer() {
	player = NewPlayer("Василий", &world, world.GetRoom("кухня"), &commands)

	initRoomEvents()
}

func initGame() {
	initWorld()
	initPlayer()
}

// Handles command
func handleCommand(input string) string {
	command := strings.Fields(input)

	if len(command) == 0 {
		return "команда не задана"
	}

	return commands.DoCommand(command[0], &player, command[1:]...)
}

func main() {
	initGame()

	var command string

	var reader *bufio.Reader = bufio.NewReader(os.Stdin)

	for true {
		fmt.Print("Введите команду: ")

		command, _ = reader.ReadString('\n')
		command = strings.TrimRight(command, "\n\r ")

		if command == "выход" {
			break
		}

		fmt.Println(handleCommand(command))
	}

}
