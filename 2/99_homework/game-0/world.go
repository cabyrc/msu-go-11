package main

// World is actually a map of rooms
type World map[string]*Room

func (w *World) AddRoom(r *Room) *Room {
	if _, ok := (*w)[r.name]; ok {
		panic("Room " + r.name + " already exists")
	}
	(*w)[r.name] = r

	return r
}

func (w *World) HasRoom(name string) bool {
	_, ok := (*w)[name]

	return ok
}

func (w *World) GetRoom(name string) *Room {
	if !w.HasRoom(name) {
		panic("Room " + name + " does not exist")
	}

	return (*w)[name]
}
