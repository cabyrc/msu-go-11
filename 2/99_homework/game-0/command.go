package main

import "fmt"

// Describes command handler function
type ActionFunc func(player *Player, args ...string) (result string, success bool)

// Describes single user command
type Command struct {
	// command name (should be lowercase and contain only letters)
	name string
	// how many arguments the command accepts
	argLength int
	// command handler
	action ActionFunc
}

// Describes a list of user commands
type CommandMap struct {
	// map of user commands, where key is a command name
	commands map[string]*Command
	// holds a structure that lets us sending and receiving events
	events *EventMap
}

// Create an empty list of commands
func NewCommandMap(eventMap *EventMap) CommandMap {
	return CommandMap{
		commands: map[string]*Command{},
		events:   eventMap,
	}
}

// Add a new command to the list
func (cm *CommandMap) AddCommand(name string, argLength int, action ActionFunc) *Command {
	cm.commands[name] = &Command{
		name:      name,
		argLength: argLength,
		action:    action,
	}

	return cm.commands[name]
}

// Check if a command exists
func (cm *CommandMap) HasCommand(name string) bool {
	_, ok := cm.commands[name]

	return ok
}

// Execute a given command
//
// Possible return values:
// - неизвестная команда // in case of non-registered command
// - неверное количество аргументов // in case of arguments number not equal to argLength
//
// Other result values are produced by the command itself
//
// This function can also produce panic, if SendEvent("pre:команда" + command) returns anything else than string
func (cm *CommandMap) DoCommand(command string, player *Player, args ...string) (result string) {
	if len(command) == 0 || !cm.HasCommand(command) {
		return "неизвестная команда"
	}

	if len(args) != cm.commands[command].argLength {
		return "неверное количество аргументов"
	}

	tmpres := cm.events.SendEvent("pre:команда:"+command, player, "", args...)

	result, ok := tmpres.(string)

	if !ok {
		panic(fmt.Sprintf("Unexpected result type %T", tmpres))
	}

	if result != "" {
		return result
	}

	result, ok = cm.commands[command].action(player, args...)

	if ok {
		cm.events.SendEvent("команда:"+command, player, result, args...)
	}

	return result
}
