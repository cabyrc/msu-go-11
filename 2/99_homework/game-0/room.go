package main

import "strings"
import "fmt"

type Room struct {
	name          string
	inRoomMsg     string
	objects       map[string]*Object
	pathNames     []string
	paths         map[string]*Room
	locationNames []string
	locations     map[string]*ObjectLocation
	events        *EventMap
	properties    map[string]string
}

type ObjectLocation struct {
	name        string
	objectNames []string
	objects     map[string]*Object
	room        *Room
}

func (r *ObjectLocation) hasObject(name string) bool {
	_, ok := r.objects[name]

	return ok
}

func (r *ObjectLocation) AddObject(object *Object) {
	r.objects[object.name] = object
	r.objectNames = append(r.objectNames, object.name)
	r.room.objects[object.name] = object
}

func (r *ObjectLocation) DeleteObject(name string) {
	delete(r.objects, name)

	slicePos := -1

	for key, val := range r.objectNames {
		if name == val {
			slicePos = key
		}
	}

	if slicePos > -1 {
		r.objectNames = append(r.objectNames[:slicePos], r.objectNames[slicePos+1:]...)
	}
}

func (r *Room) SetProperty(name string, value string) *Room {
	r.properties[name] = value

	return r
}

func (r *Room) GetProperty(name string) (value string, ok bool) {
	value, ok = r.properties[name]

	return
}

func (r *Room) ConnectTo(r2 *Room, alias string) {
	r.paths[alias] = r2
	r.pathNames = append(r.pathNames, alias)
}

func (r *Room) HasConnectedRoom(name string) bool {
	_, ok := r.paths[name]

	return ok
}

func (r *Room) GetConnectedRoom(alias string) *Room {
	return r.paths[alias]
}

func (r *Room) HasObject(name string) bool {
	_, ok := r.objects[name]

	return ok
}

func (r *Room) PickObject(name string) *Object {
	result := r.objects[name]

	if result != nil {
		delete(r.objects, name)

		var location *ObjectLocation

		for _, val := range r.locations {
			if val.hasObject(name) {
				location = val
				break
			}
		}

		if location != nil {
			location.DeleteObject(name)
		}
	}

	return result
}

func (r *Room) GetInRoomMsg() string {
	return r.inRoomMsg + " можно пройти - " + strings.Join(r.pathNames, ", ")
}

func (r *Room) GetLookAroundMsg() string {
	description, ok := r.GetProperty("описание")

	if !ok {
		panic(fmt.Sprintf("Description for room \"%s\" does not exist)", r.name))
	}

	return description + " можно пройти - " + strings.Join(r.pathNames, ", ")
}

func (r *Room) AddObjectLocation(name string) *ObjectLocation {
	if _, ok := r.locations[name]; ok {
		panic("Location " + name + " already exists")
	}

	var location = &ObjectLocation{
		name:    name,
		room:    r,
		objects: map[string]*Object{},
	}

	r.locations[name] = location
	r.locationNames = append(r.locationNames, location.name)

	return location
}

func (r *Room) wrapEventHandler(callback EventCallback) EventCallback {
	return func(player *Player, context interface{}, args ...string) (result interface{}) {
		result = context
		if player.room != r {
			return
		}

		return callback(player, context, args...)
	}
}

func (r *Room) BindEventHandler(name string, callback EventCallback) {
	events.BindEvent(name, r.wrapEventHandler(callback))
}

func NewRoom(name string, inRoomMsg string, events *EventMap) *Room {
	return &Room{
		name:          name,
		inRoomMsg:     inRoomMsg,
		paths:         map[string]*Room{},
		pathNames:     []string{},
		locations:     map[string]*ObjectLocation{},
		locationNames: []string{},
		events:        events,
		objects:       map[string]*Object{},
		properties:    map[string]string{},
	}
}
