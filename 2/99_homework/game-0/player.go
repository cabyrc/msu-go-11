package main

import "strings"

type Player struct {
	name      string
	world     *World
	room      *Room
	inventory *Inventory
	commands  *CommandMap
}

type playerCommand struct {
	argLength int
	handler   func(player *Player, args ...string) (result string, success bool)
}

func newPlayerCommands() map[string]playerCommand {
	var playerCommands map[string]playerCommand

	playerCommands = map[string]playerCommand{
		"инвентарь": {
			argLength: 0,
			handler: func(player *Player, args ...string) (result string, success bool) {
				if player.inventory == nil {
					return "место для инвентаря отсутствует", false
				}

				var objects = []string{}

				for key := range *player.inventory {
					objects = append(objects, key)
				}

				result = strings.Join(objects, ", ")

				if result == "" {
					result = "инвентарь пуст"
				}

				return result, true
			},
		},
		"осмотреться": {
			argLength: 0,
			handler: func(player *Player, args ...string) (result string, success bool) {
				return player.room.GetLookAroundMsg(), true
			},
		},
		"идти": {
			argLength: 1,
			handler: func(player *Player, args ...string) (result string, success bool) {
				if !player.GoToRoom(args[0]) {
					return "нет пути в " + args[0], false
				}
				return player.room.GetInRoomMsg(), true
			},
		},
		"надеть": {
			argLength: 1,
			handler: func(player *Player, args ...string) (result string, success bool) {
				return "нет такого", false
			},
		},
		"взять": {
			argLength: 1,
			handler: func(player *Player, args ...string) (result string, success bool) {
				if player.inventory == nil {
					return "некуда класть", false
				}

				result = "нет такого"
				success = false

				var object = player.PickObject(args[0])

				if object != nil {
					result = "предмет добавлен в инвентарь: " + object.name
					success = true
				}

				return
			},
		},
		"применить": {
			argLength: 2,
			handler: func(player *Player, args ...string) (result string, success bool) {
				if player.inventory == nil || !player.inventory.hasObject(args[0]) {
					return "нет предмета в инвентаре - " + args[0], false
				}

				return "не к чему применить", false
			},
		},
		"выход": {
			argLength: 0,
			handler:   func(player *Player, args ...string) (result string, success bool) { return "", false },
		},
		"помощь": {
			argLength: 0,
			handler: func(player *Player, args ...string) (result string, success bool) {
				keys := make([]string, len(playerCommands))

				i := 0
				for k := range playerCommands {
					keys[i] = k
					i++
				}

				return "Доступные команты: " + strings.Join(keys, ", "), true
			},
		},
	}

	return playerCommands
}

func NewPlayer(name string, world *World, room *Room, commands *CommandMap) (player Player) {
	player = Player{
		name:     name,
		world:    world,
		room:     room,
		commands: commands,
	}

	for cmd, data := range newPlayerCommands() {
		player.commands.AddCommand(cmd, data.argLength, data.handler)
	}

	return
}

func (p *Player) CanGoToRoom(name string) bool {
	if !p.room.HasConnectedRoom(name) {
		return false
	}

	return true
}

func (p *Player) GoToRoom(name string) bool {
	if !p.CanGoToRoom(name) {
		return false
	}

	p.room = p.room.GetConnectedRoom(name)

	return true
}

func (p *Player) CanPickObject(name string) bool {
	if !p.room.HasObject(name) {
		return false
	}

	return true
}

func (p *Player) PickObject(name string) *Object {
	if p.inventory == nil {
		return nil
	}

	if !p.CanPickObject(name) {
		return nil
	}

	var object = p.room.PickObject(name)

	if object == nil {
		return nil
	}

	(*p.inventory)[name] = object

	return object
}
