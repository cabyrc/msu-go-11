package main

import (
	"time"
)

type Calendar struct {
    time.Time
}

func NewCalendar(t time.Time) Calendar {
    return Calendar{t}
}


// TODO Реализовать Календарь
func (c *Calendar) CurrentQuarter() (result int) {
    result = (int(c.Month()) + 2) / 3
    return
}

func main() {
}